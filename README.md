# To-Do List RESTful API
This is a simple RESTful API for managing a to-do list. The API allows you to create, read, update, mark as finished, and soft delete to-do items. It's built using FastAPI in Python.


## [ Installation and Setup ]

### 1. Clone this repository to your local machine:
```url
$ git clone https://gitlab.com/AnasR7/mission-todolist.git
```

### 2. To Install the Package use pipenv or pip

- pipenv:
```term
$ pipenv install -r requirements.txt
```
- pip:
```term
$ pip install -r requirements.txt
```

### 3. run the server
```term
$ uvicorn main:app --reload
```


The API will be available at `http://localhost:8000`.

## [ Usage ]

You can use tools like [Postman](https://www.postman.com/) to interact with the API. Here are some example requests:

- **Create a To-Do Item (POST):**

```http
POST http://localhost:8000/todo/
{
   "title": "Sample Task",
   "description": "This is a sample task."
}

- **Get All To-Do Items (GET):**

  ```http
  GET http://localhost:8000/todo/
  ```

- **Get a To-Do Item by ID (GET):**

  ```http
  GET http://localhost:8000/todo/1
  ```

- **Update a To-Do Item (PUT):**

  ```http
  PUT http://localhost:8000/todo/1
  {
      "title": "Updated Task",
      "description": "This is an updated task."
  }
  ```

- **Mark a To-Do Item as Finished (PUT):**

  ```http
  PUT http://localhost:8000/todo/1/finish
  ```

- **Soft Delete a To-Do Item (DELETE):**

  ```http
  DELETE http://localhost:8000/todo/1
  ```
