from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from typing import List, Optional
from datetime import datetime

app = FastAPI()

# Define the Todo data model with title and description fields
class Todo(BaseModel):
    title: str
    description: str

# Extend the Todo data model to include additional fields like id and timestamps
class TodoItem(Todo):
    id: int
    created_at: datetime
    updated_at: datetime
    finished_at: Optional[datetime]
    deleted_at: Optional[datetime]

# Define a custom response model that includes a message along with the data
class TodoResponse(BaseModel):
    message: str
    data: TodoItem

# Create an empty list to store todo items in memory
todos = []

# Define an endpoint to create a new todo item
@app.post("/todo/", response_model=TodoResponse)
def create_todo(todo: Todo):
    # Create a new todo item with unique id and timestamps
    todo_item = TodoItem(
        id=len(todos) + 1,
        created_at=datetime.now(),
        updated_at=datetime.now(),
        finished_at=None,
        deleted_at=None,
        **todo.dict(),
    )
    # Add the todo item to the list
    todos.append(todo_item)
    
    # Prepare a response message
    response_message = "Your todo was successfully added."
    
    # Return a JSON response with the message and the created todo item
    return TodoResponse(message=response_message, data=todo_item)

# Define an endpoint to retrieve all todo items
@app.get("/todo/", response_model=List[TodoItem])
def get_all_todos():
    # Return all non-deleted todo items
    return [todo for todo in todos if not todo.deleted_at]

# Define an endpoint to retrieve a specific todo item by its ID
@app.get("/todo/{id}", response_model=TodoResponse)
def get_todo_by_id(id: int):
    # Find the todo item by its ID and ensure it's not deleted
    todo = next((todo for todo in todos if todo.id == id and not todo.deleted_at), None)
    if not todo:
        # If not found, raise a 404 error
        raise HTTPException(status_code=404, detail="Todo not found")
    
    # Prepare a response message
    response_message = "Todo found."
    
    # Return a JSON response with the message and the todo item
    return TodoResponse(message=response_message, data=todo)

# Define an endpoint to update a todo item by its ID
@app.put("/todo/{id}", response_model=TodoResponse)
def update_todo(id: int, todo: Todo):
    # Find the todo item by its ID and ensure it's not deleted
    todo_to_update = next((t for t in todos if t.id == id and not t.deleted_at), None)
    if not todo_to_update:
        # If not found, raise a 404 error
        raise HTTPException(status_code=404, detail="Todo not found")
    
    # Update the todo item's title, description, and timestamps
    todo_to_update.title = todo.title
    todo_to_update.description = todo.description
    todo_to_update.updated_at = datetime.now()
    
    # Prepare a response message
    response_message = "Todo updated successfully."
    
    # Return a JSON response with the message and the updated todo item
    return TodoResponse(message=response_message, data=todo_to_update)

# Define an endpoint to partially update a todo item by its ID
@app.patch("/todo/{id}", response_model=TodoResponse)
def partial_update_todo(id: int, todo: Todo):
    # Find the todo item by its ID and ensure it's not deleted
    todo_to_update = next((t for t in todos if t.id == id and not t.deleted_at), None)
    if not todo_to_update:
        # If not found, raise a 404 error
        raise HTTPException(status_code=404, detail="Todo not found")
    
    # Partially update the todo item's title, description, and timestamps
    for key, value in todo.dict().items():
        if key == 'title':
            todo_to_update.title = value
        elif key == 'description':
            todo_to_update.description = value
    todo_to_update.updated_at = datetime.now()
    
    # Prepare a response message
    response_message = "Todo updated successfully."
    
    # Return a JSON response with the message and the updated todo item
    return TodoResponse(message=response_message, data=todo_to_update)

# Define an endpoint to mark a todo item as finished by its ID
@app.put("/todo/{id}/finish", response_model=TodoResponse)
def finish_todo(id: int):
    # Find the todo item by its ID and ensure it's not deleted
    todo_to_finish = next((t for t in todos if t.id == id and not t.deleted_at), None)
    if not todo_to_finish:
        # If not found, raise a 404 error
        raise HTTPException(status_code=404, detail="Todo not found")
    
    # Update the todo item to mark it as finished and update timestamps
    todo_to_finish.finished_at = datetime.now()
    todo_to_finish.updated_at = datetime.now()
    
    # Prepare a response message
    response_message = "Todo marked as finished."
    
    # Return a JSON response with the message and the updated todo item
    return TodoResponse(message=response_message, data=todo_to_finish)

# Define an endpoint to soft delete a todo item by its ID
@app.delete("/todo/{id}", response_model=TodoResponse)
def soft_delete_todo(id: int):
    # Find the todo item by its ID and ensure it's not deleted
    todo_to_delete = next((t for t in todos if t.id == id and not t.deleted_at), None)
    if not todo_to_delete:
        # If not found, raise a 404 error
        raise HTTPException(status_code=404, detail="Todo not found")
    
    # Soft delete the todo item by updating the deleted_at timestamp
    todo_to_delete.deleted_at = datetime.now()
    todo_to_delete.updated_at = datetime.now()
    
    # Prepare a response message
    response_message = "Todo has been deleted."
    
    # Return a JSON response with the message and the deleted todo item
    return TodoResponse(message=response_message, data=todo_to_delete)
